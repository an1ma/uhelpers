<?php
/**
 * Created by JetBrains PhpStorm.
 * Name: MakeSectionTree.php
 * Description: Create a historical section hierarhy for news
 * Version: 0.3
 * User: mbakirov
 * Date: 20.08.11
 * Time: 15:16
 */

$SectionTree = new SectionTree();
AddEventHandler("iblock", "OnBeforeIBlockElementAdd", array($SectionTree, "MakeSectionTree"));
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", array($SectionTree, "MakeSectionTree"));

class SectionTree {
    // Params
    var $iblockCode = "news";
    var $modules = array("iblock");
    var $sortStart = 5000;

    var $APP = false;
    var $time = false;
    var $topLevelSection = false;
    var $secondLevelSection = false;
    var $iblock = false;

    function __construct() {
        if(is_array($this->modules)) {
            foreach($this->modules as $key => $module) {
                CModule::IncludeModule($module);
            }
        }
        global $APPLICATION;
        $this->APP = $APPLICATION;
        $this->time = time();
    }

    function CreateTopLevelSection($time) {
        // Making date
        $year = date("Y", $time);
        $arMonthes = array("Нулябрь","Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь");

        //checking year section
        $dbYearSection = CIBlockSection::GetList(array(), array("NAME" => $year, "IBLOCK_ID" => $this->iblock["ID"]));
        if(!$this->topLevelSection = $dbYearSection->GetNext()) {
            $CIBlocSection = new CIBlockSection();
            $sort = $this->sortStart - $year;
            $this->topLevelSection["ID"] = $CIBlocSection->Add(array("NAME" => $year, "CODE" => $year, "SORT" => $sort, "IBLOCK_ID" => $this->iblock["ID"]));
        } else {
            if($this->topLevelSection["ACTIVE"] !== "Y") {
                $CIBlocSection = new CIBlockSection();
                $CIBlocSection->Update($this->topLevelSection["ID"], array("ACTIVE" => "Y"));
            }
        }
    }

    function CreateSecondLevelSection($time) {
        $arMonthes = array("Нулябрь","Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь");
        $month = array("NAME" => $arMonthes[date("n", $time)], "CODE" => strtolower(date("F", $time)));

        //checking child month section
        $dbMonthSection = CIBlockSection::GetList(array(), array("SECTION_ID" => $this->topLevelSection["ID"], "NAME" => $month["NAME"], "IBLOCK_ID" => $this->iblock["ID"]));
        if(!$this->secondLevelSection = $dbMonthSection->GetNext()) {
            $CIBlocSection = new CIBlockSection();
            $sort = array_search($month["NAME"], $arMonthes);
            $arMonthSection["ID"] = $CIBlocSection->Add(array("NAME" => $month["NAME"], "SORT" => $sort, "CODE" => $this->topLevelSection["CODE"]."-".$month["CODE"], "IBLOCK_ID" => $this->iblock["ID"], "IBLOCK_SECTION_ID" => $this->topLevelSection["ID"]));
        } else {
            if($this->secondLevelSection["ACTIVE"] !== "Y") {
                $CIBlocSection = new CIBlockSection();
                $CIBlocSection->Update($this->secondLevelSection["ID"], array("ACTIVE" => "Y"));
            }
        }
    }

    function MakeSectionTree (&$arFields) {
        $IBLOCK_CODE = $this->iblockCode;
        $this->iblock = CIBlock::GetList(array(), array("CODE" => $IBLOCK_CODE))->GetNext();
        if($arFields["IBLOCK_ID"] != $this->iblock["ID"]) {
            return $arFields;
        }

        // Getting additional info
        $dbSection = CIBlockElement::GetByID($arFields["ID"]);
        if($arSection = $dbSection->GetNext()) {
            // If we update existing section
            if($arFields["ACTIVE_FROM"])
                $time = strtotime($arFields["ACTIVE_FROM"]);
            else
                $time = strtotime($arSection["DATE_CREATE"]);
        } else {
            // If we create new one
            if($arFields["ACTIVE_FROM"])
                $time = strtotime($arFields["ACTIVE_FROM"]);
            else
                return false;
        }

        $this->CreateTopLevelSection($time);

        if(is_array($this->topLevelSection)) {
            $this->CreateSecondLevelSection($time);
        }

        if($this->topLevelSection["ID"] && $this->secondLevelSection["ID"]) {
            $arFields["IBLOCK_SECTION"][0] = $this->secondLevelSection["ID"];
        }
    }
}