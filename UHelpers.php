<?
/**
 * Created by JetBrains PhpStorm.
 * Name: UHelpers.php
 * Description: some usefull stuff
 * Version: 0.3
 * User: mbakirov
 * Date: 25.04.12
 * Time: 12:32
 */

Class UHelpers {
    /*
     * @method varchar(250) CreateCode()
     * @method array ResizeImage()
     * @method array GetLinkedPacket()
     * @method string GetCurPageParam()
     * @method redirect CheckGetParams()
     */
    static public function Transliterate($string, $defaultSpacer = "_", $strlen = 250) {
        if(strlen($string) < 1)
            return false;

        //translit
        $translit = array("а"=>"a", "б"=>"b", "в"=>"v", "г"=>"g", "д"=>"d", "е"=>"e", "Є"=>"e", "ж"=>"zh", "з"=>"z", "и"=>"i", "й"=>"y", "к"=>"k", "л"=>"l", "м"=>"m", "н"=>"n", "о"=>"o", "п"=>"p", "р"=>"r", "с"=>"s", "т"=>"t", "у"=>"u", "ф"=>"f", "х"=>"h", "ц"=>"c", "ч"=>"ch", "ш"=>"sh", "щ"=>"sh", "ы"=>"y", "ь"=>"", "ъ"=>"", "э"=>"e", "ю"=>"u", "я"=>"ya");
        $string = strtr(strtolower($string),$translit);

        //changin spec.symbols if they still here
        $find = array(" ", "-", ".", ",", ":", ";", "+", "=", "@", "!", "~", "`", '"', "'", "$", "#", "%", "^", "&", "?", "*", "(", ")", "\\", "/", "|", "[", "]", "{", "}", "<", ">", "№", );
        $string = str_replace($find, $defaultSpacer, $string);

        //checking lead and last "_"
        $defaultSpacer_safe = "\\".$defaultSpacer;
        $string = preg_replace("/^".$defaultSpacer_safe."+/", "", preg_replace("/".$defaultSpacer_safe."+$/", "", $string));

        //checking double or more "_"
        $string = preg_replace("/(\_{2,})/", $defaultSpacer, $string);

        if(intval($strlen) > 0) {
            if(strlen($string) > intval($strlen)) $string = substr($string, 0, intval($strlen));
        }

        return $string;
    }

    /*
     * @param mixed $rsPicture ID картинки или массив, описывающий картинку
     * @param array $arResizeProportions  массив, описывающий ширину и высоту картинки array('height'=>100, 'width'=>100);
     * @param constant $typeOfResize будет использован, если не задан один из параметров $arResizeProportions. По умолчанию BX_RESIZE_IMAGE_EXACT (расчет пропорций проводится внутри функции)
     * @return array();
     */
    static public function ResizeImage($rsPicture, $arResizeProportions, $typeOfResize = BX_RESIZE_IMAGE_EXACT) {
        if(empty($arResizeProportions)||(!array_key_exists("min-width", $arResizeProportions) &&!array_key_exists("width", $arResizeProportions))||(!array_key_exists("min-height", $arResizeProportions) &&!array_key_exists("height", $arResizeProportions))) return false;

        if(is_array($rsPicture) && intval($rsPicture["WIDTH"]) > 0 && intval($rsPicture["HEIGHT"]) > 0 && intval($rsPicture["ID"]) > 0 && strlen($rsPicture["SRC"]) > 0) {
            $arFile = $rsPicture;
        } elseif(!is_array($rsPicture) && intval($rsPicture) > 0) {
            $arFile = CFile::GetFileArray(intval($rsPicture));
        } else {
            return false;
        }

        if(array_key_exists('min-width', $arResizeProportions) && array_key_exists('min-height', $arResizeProportions)) {
            $arResizeProportions['width'] = $arResizeProportions['min-width'];
            $arResizeProportions["height"] = ceil($arResizeProportions['width'] * $arFile["HEIGHT"] / $arFile["WIDTH"]);

            if($arResizeProportions["height"] < $arResizeProportions['min-height']) {
                $arResizeProportions['height'] = $arResizeProportions['min-height'];
                $arResizeProportions["width"] = ceil($arResizeProportions['height'] * $arFile["WIDTH"] / $arFile["HEIGHT"]);
            }
            $bMinDimensions = true;
        } elseif($arFile["WIDTH"] > $arResizeProportions["width"] || $arFile["HEIGHT"] > $arResizeProportions["height"]) {
            if(!$arResizeProportions["width"]) {
                $props = $arFile["HEIGHT"] / $arResizeProportions['height'];
                $arResizeProportions["width"] = ceil($arFile["WIDTH"] / $props);
            }
            if(!$arResizeProportions["height"]) {
                $props = $arFile["WIDTH"] / $arResizeProportions['width'];
                $arResizeProportions["height"] = ceil($arFile["HEIGHT"] / $props);
            }
        } else {
            return false;
        }

        if($typeOfResize != BX_RESIZE_IMAGE_EXACT) {
            if($arResizeProportions['height'] > $arResizeProportions['width']) {
                $typeOfResize = BX_RESIZE_IMAGE_PROPORTIONAL_ALT;
            } else {
                $typeOfResize = BX_RESIZE_IMAGE_PROPORTIONAL;
            }
        }

        //unset($arResizeProportions['min-width'], $arResizeProportions['min-height']);

        $arResize = CFile::ResizeImageGet(
            $arFile["ID"],
            array(
                'width' => $arResizeProportions['width'],
                'height' => $arResizeProportions['height']
            ),
            $typeOfResize,
            true
        );

        $arFile["WIDTH"] = $arResize["width"];
        $arFile["HEIGHT"] = $arResize["height"];
        $arFile["ORIGINAL_SRC"] = $arFile["SRC"];
        $arFile["SRC"] = $arResize["src"];

        if($bMinDimensions) {
            $arFile['ADD_CSS'] = '';
            $arFile['ARR_ADD_CSS'] = array();
            if($arFile["HEIGHT"] > $arResizeProportions['min-height']) {
                $arFile['ARR_ADD_CSS']['TOP'] = ceil(-1*($arFile["HEIGHT"]-$arResizeProportions['min-height'])/2).'px';
                $arFile['ADD_CSS'].='margin-top:'.ceil(-1*($arFile["HEIGHT"]-$arResizeProportions['min-height'])/2).'px;';
            }
            if($arFile['WIDTH'] > $arResizeProportions['min-width']) {
                $arFile['ARR_ADD_CSS']['LEFT'] = ceil(-1*($arFile["WIDTH"]-$arResizeProportions['min-width'])/2).'px';
                $arFile['ADD_CSS'].='margin-left:'.ceil(-1*($arFile["WIDTH"]-$arResizeProportions['min-width'])/2).'px;';
            }
        }

        return $arFile;
    }


    /**
     * Множественное число
     *
     * @param int $howmuch
     * @param array $input Например: [0] - товар (единственное число, именительный падеж), [1] - товара (единственное число, родительный падеж), [2] - товаров (множественное число, родительный падеж)
     * @return string
     */
    static public function Plural($howmuch, $input) {
        $howmuch = (int)$howmuch;
        $l2 = substr($howmuch,-2);
        $l1 = substr($howmuch,-1);
        if($l2 > 10 && $l2 < 20) return $input[2];
        else
            switch ($l1) {
                case 0: return $input[2]; break;
                case 1: return $input[0]; break;
                case 2: case 3: case 4: return $input[1]; break;
                default: return $input[2]; break;
            }
    }

    /**
     * Кодировка в JSON с поддержкой русских символов
     * @param $str
     * @return string
     */
    static function json_encode($str) {
        $arr_replace_utf = array('\u0410', '\u0430','\u0411','\u0431','\u0412','\u0432',
            '\u0413','\u0433','\u0414','\u0434','\u0415','\u0435','\u0401','\u0451','\u0416',
            '\u0436','\u0417','\u0437','\u0418','\u0438','\u0419','\u0439','\u041a','\u043a',
            '\u041b','\u043b','\u041c','\u043c','\u041d','\u043d','\u041e','\u043e','\u041f',
            '\u043f','\u0420','\u0440','\u0421','\u0441','\u0422','\u0442','\u0423','\u0443',
            '\u0424','\u0444','\u0425','\u0445','\u0426','\u0446','\u0427','\u0447','\u0428',
            '\u0448','\u0429','\u0449','\u042a','\u044a','\u042b','\u044b','\u042c','\u044c',
            '\u042d','\u044d','\u042e','\u044e','\u042f','\u044f');
        $arr_replace_cyr = array('А', 'а', 'Б', 'б', 'В', 'в', 'Г', 'г', 'Д', 'д', 'Е', 'е',
            'Ё', 'ё', 'Ж','ж','З','з','И','и','Й','й','К','к','Л','л','М','м','Н','н','О','о',
            'П','п','Р','р','С','с','Т','т','У','у','Ф','ф','Х','х','Ц','ц','Ч','ч','Ш','ш',
            'Щ','щ','Ъ','ъ','Ы','ы','Ь','ь','Э','э','Ю','ю','Я','я');
        $str1 = json_encode($str);
        $str2 = str_replace($arr_replace_utf,$arr_replace_cyr,$str1);
        return $str2;
    }
}