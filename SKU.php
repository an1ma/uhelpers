<?php
/**
 * Created by JetBrains PhpStorm.
 * Name: SKU.php
 * Description: Create multiple SKUs
 * Version: 0.2
 * User: mbakirov
 * Date: 20.08.11
 * Time: 15:16
 */

AddEventHandler('iblock', 'OnAfterIBlockElementUpdate', array('SKU','CreateMultipleSKU'));
AddEventHandler('iblock', 'OnAfterIBlockElementAdd', array('SKU','CreateMultipleSKU'));

Class SKU {
    var $catalog_iblock_id = 1;

    public function CreateMultipleSKU ($arFields) {
        $catalog_iblock_id = 1;

        CModule::IncludeModule("iblock");
        $arResult = array(
            'catalog_iblock_id' => '',
            'sku_iblock_id' => '',
            'system_properties' => array(),
            'sku' => array(),
        );

        global $APPLICATION;

        // Getting IBLOCKs IDs
        $dbElement = CIBlockElement::GetList(array(), array("ID" => $arFields["ID"], "IBLOCK_ID" => $arFields['IBLOCK_ID']), false, false, array('IBLOCK_ID','ID','NAME','CODE','CATALOG_GROUP_1','ACTIVE'));
        $obElement = $dbElement->GetNextElement();
        $arElement = $obElement->GetFields();
        $arResult["catalog_iblock_id"] = $arElement["IBLOCK_ID"];

        // Если изменения не в каталоге, то шлем всех нефиг
        if($arResult["catalog_iblock_id"] != $catalog_iblock_id) return false;

        // Находим Инфоблок Торговых предложений
        $dbCatalog = CCatalog::GetList(array(), array('PRODUCT_IBLOCK_ID' => $arResult["catalog_iblock_id"]));
        if($arCatalog = $dbCatalog->GetNext()) {
            $arResult['sku_iblock_id'] = $arCatalog["IBLOCK_ID"];
        } else {
            $exception = new CAdminException(array());
            $exception->AddMessage(array("text" => "Не могу найти инфоблок торговых предложений."));
            $APPLICATION->throwException($exception);
            return false;
        }

        // Добываем значения свойств в удобоваримый вид
        $arElement["PROPERTIES"] = $obElement->GetProperties(array(), array('CODE' => "SYSTEM_%"));
        foreach($arElement['PROPERTIES'] as $code => $arProperty) {
            $code = str_replace("SYSTEM_", "", $code);
            if(is_array($arProperty["VALUE"])) {
                $arResult["system_properties"]["values"][$code] = $arProperty["VALUE"];
            } elseif(strlen($arProperty["VALUE"])) {
                $arResult["system_properties"]["values"][$code][0] = $arProperty["VALUE"];
            }
        }

        // Комбинируем значения свойств и проверяем, чтоб что-то да осталось (для товаров с одним свойством)
        self::ArrayCombine(array_keys($arResult["system_properties"]["values"]), $arResult["system_properties"]["values"], $arResult['sku'], array());
        if(count($arResult['sku']) < 1) {
            foreach($arResult['system_properties']['values'] as $propCode => $prop) {
                if(count($prop) > 0) {
                    foreach($prop as $value) {
                        $temp[$propCode] = $value;
                        $arResult['sku'][] = $temp;
                    }
                }
            }
        }

        if(count($arResult['sku']) < 1) {
            $exception = new CAdminException(array());
            $exception->AddMessage(array("text" => "Должно быть заполнено хотя бы одно системное свойство (размер, цвет)"));
            $APPLICATION->throwException($exception);
            return false;
        }

        // Удаляем СКУшки
        $dbSKU = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $arResult['sku_iblock_id'], 'PROPERTY_CML2_LINK' => $arElement["ID"]));
        while($arSKU = $dbSKU->GetNext()) {
            $CIBlockElement = new CIBlockElement();
            $CIBlockElement->Delete($arSKU["ID"]);
        }

        // Создаем СКУшки
        foreach($arResult['sku'] as $arSKU) {
            $arSKU['CML2_LINK'] = $arElement["ID"];
            $arAddFields = array(
                'IBLOCK_ID' => $arResult['sku_iblock_id'],
                'NAME' => $arElement['NAME'],
                'ACTIVE' => $arElement["ACTIVE"],
                "PROPERTY_VALUES" => $arSKU
            );
            $CIBlockElement = new CIBlockElement();
            if($SKU_ID = $CIBlockElement->Add($arAddFields)) {
                CCatalogProduct::Add(
                    array(
                        "ID" => $SKU_ID,
                        "QUANTITY" => 0,
                        "QUANTITY_TRACE" => "N",
                        "WEIGHT" => 0,
                        "PRICE_TYPE" => "S",
                        "RECUR_SCHEME_TYPE" => "D",
                        "RECUR_SCHEME_LENGTH" => 0,
                        "TRIAL_PRICE_ID" => 0,
                        "WITHOUT_ORDER" => "N"
                    )
                );
                CPrice::Add(
                    array(
                        "PRODUCT_ID" => $SKU_ID,
                        "EXTRA_ID" => false,
                        "CATALOG_GROUP_ID" => 1,
                        "PRICE" => $arElement['CATALOG_PRICE_1'],
                        'CURRENCY' => $arElement['CATALOG_CURRENCY_1'],
                    )
                );
            } else {
                $APPLICATION->ThrowException('По какой-то неведомой причине не смог создать SKU. Обратитесь к разработчику');
                return false;
            }
        }

        return true;
    }

    public function ArrayCombine($keys, $array, &$result, $path) {
        if(count($keys) > 1) {
            $key = array_pop($keys);
            foreach($array[$key] as $k => $v) {
                self::ArrayCombine($keys, $array, $result, array_merge($path, array($key=>$v)));
            }
        } else {
            $key = array_pop($keys);
            foreach ($array[$key] as $k=>$v)
                $result[] = array_merge($path,array($key=>$v));
        }
    }

    public function GetElementID($product_id) {
        CModule::IncludeModule("iblock");
        $dbElement = CIBlockElement::GetByID($product_id);
        if($obElement = $dbElement->GetNextElement()) {
            $CML2_LINK = $obElement->GetProperty("CML2_LINK");
            if(is_array($CML2_LINK) && $CML2_LINK["VALUE"]) {
                return $CML2_LINK["VALUE"];
            } else {
                $arFields = $obElement->GetFields();
                return $arFields["ID"];
            }
        } else {
            return false;
        }
    }
}
